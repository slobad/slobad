import pandas as pd
from slobad import Slobad


class SlobadConfig(Slobad):
    #create a common base object to avoid reptition of configuration
    #could also write cache_dir in each class
    cache_dir = 'cache'
    filetype = 'feather'


class CreateData(SlobadConfig):
    #each SlobabBase class requires a create method
    def create(self):
        data = [
            ['Mary', 10], ['Edith', 8], ['Sybil', 6],
            ['Mary', 20], ['Edith', 16], ['Sybil', 12],
        ]
        df = pd.DataFrame(data, columns=['Name', 'Age'])
        return df


class TransformData(SlobadConfig):
    def create(self, df_input):
        df = df_input.groupby('Name')['Age'].mean()
        #cannot write a series to feather
        df = df.reset_index()
        return df


class TransformAgain(Slobad):
    #use a different filetype, don't use configuration object
    cache_dir = 'cache'
    filetype = 'csv'

    def create(self, df_arg):
        df = df_arg.groupby('Name')['Age'].max()
        return df


def main():
    df1 = CreateData().run()
    df2 = TransformData().run(df1)
    df3 = TransformAgain().run(df1)


if __name__ == '__main__':
    main()
