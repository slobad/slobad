import pandas as pd
from slobad import Slobad


class CreateData(Slobad):
    #each Slobad class requires a create method
    def create(self):
        data = [[0] * 10] * (10 ** 6)
        cols = [f'{i}' for i in range(10)]
        df = pd.DataFrame(data, columns=cols)
        return df

df = CreateData().run()
cached = CreateData().run()
print(df.equals(cached))
