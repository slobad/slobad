import pandas as pd
from slobad import Slobad
import traceback


class NoneReturn(Slobad):
    def create(self):
        return None


class EmptyFrame(Slobad):
    def create(self):
        return pd.DataFrame()


class Filetype(Slobad):
    filetype = 'gobbledygook'
    def create(self):
        data = [[0] * 10] * (10 ** 2)
        cols = [f'{i}' for i in range(10)]
        df = pd.DataFrame(data, columns=cols)
        return df


badbad = [NoneReturn, EmptyFrame, Filetype]
for bad in badbad:
    try:
        bad().run()
    except Exception as e:
        print(traceback.format_exc())
