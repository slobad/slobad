import pandas as pd
import os
from slobad import Slobad


class CreateData(Slobad):
    cache_dir = 'cache'
    filetype = 'feather'

    def create(self):
        data = [
            ['Mary', 10], ['Edith', 8], ['Sybil', 6],
        ]
        df = pd.DataFrame(data, columns=['Name', 'Age'])
        df['Year'] = 2016
        return df


class Parameterized(Slobad):
    cache_dir = 'cache'
    filetype = 'feather'

    def __init__(self, year):
        self.year = year

    @property
    def name(self):
        return self.__class__.__name__ + str(self.year)

    def create(self, df):
        df[f'Age_{self.year}'] = df['Age'] + self.year - df['Year']
        df['Year'] = self.year
        df = df.drop('Age', axis=1)
        return df


def main():
    #to parameterize a Slobad object overwrite the name method and,
    #add paramter to object init
    df = CreateData().create()
    years = list(range(2017, 2020))
    for y in years:
        py = Parameterized(y)
        y_df = py.run(df)
    #saved fname includes the parameter from name property
    print([f for f in os.listdir('cache') if 'Parameterized' in f])


if __name__ == '__main__':
    main()
