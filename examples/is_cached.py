import pandas as pd
from slobad import Slobad


class SlobadConfig(Slobad):
    #create a common base object to avoid reptition of configuration
    #could also write cache_dir in each class
    cache_dir = 'cache'
    filetype = 'feather'


class CreateData(SlobadConfig):
    #each SlobabBase class requires a create method
    def create(self):
        data = [
            ['Mary', 10], ['Edith', 8], ['Sybil', 6],
            ['Mary', 20], ['Edith', 16], ['Sybil', 12],
        ]
        df = pd.DataFrame(data, columns=['Name', 'Age'])
        return df


class TransformData(SlobadConfig):
    def create(self, df_input):
        df = df_input.groupby('Name')['Age'].mean()
        #cannot write a series to feather
        df = df.reset_index()
        return df


def main():
    #to avoid a slow load or to save on memory of upstream artifacts
    transformdata = TransformData()
    #load createdata aritifact if transformdata artifact isn't already created
    if not transformdata.is_cached():
        df1 = CreateData().run()
        df2 = transformdata.run(df1)
    else:
        df2 = transformdata.run()


if __name__ == '__main__':
    main()
